const geofenceController = require('../controllers/geofenceController')
const express = require('express')
const router = express.Router()

router.get('/', geofenceController.geofence_list)
router.post('/', geofenceController.geofence_create_post)
router.post('/:id', geofenceController.geofence_assign_vehicle)
router.delete('/:id', geofenceController.geofence_detach_vehicle)

module.exports = router;
