const tagController = require('../controllers/tagController')
const express = require('express')
const router = express.Router()

router.get('/', tagController.tag_list)
router.get('/:id', tagController.tag_detail)
router.post('/', tagController.tag_create_post)
router.put('/:id', tagController.tag_update_post)
router.delete('/:id', tagController.tag_delete_post)

module.exports = router;
