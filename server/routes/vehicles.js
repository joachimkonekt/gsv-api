const vehicleController = require('../controllers/vehicleController')
const express = require('express')
const {route} = require("express/lib/router");
const router = express.Router()

router.get('/', vehicleController.vehicle_list)
router.get('/:id', vehicleController.vehicle_detail)
router.post('/', vehicleController.vehicle_create_post)
router.post('/', vehicleController.vehicle_create_mass)
router.put('/:id', vehicleController.vehicle_update_put)
router.delete('/:id', vehicleController.vehicle_delete_post)

router.get('/:id/tags', vehicleController.vehicle_get_tags)
router.post('/:id/tags', vehicleController.vehicle_assign_tag_post)
router.delete('/:id/tags/:tag_id', vehicleController.vehicle_detach_tag_delete)

router.get('/:id/reports/location', vehicleController.vehicle_location_report)
router.get('/:id/reports/utilization', vehicleController.vehicle_utilization_report)

router.get('/:id/location', vehicleController.vehicle_location)
router.get('/:id/utilization', vehicleController.vehicle_utilization)

router.post('/:id/consumption', vehicleController.vehicle_add_consumption)

module.exports = router;
