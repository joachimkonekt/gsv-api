const messageController = require('../controllers/messageController')
const express = require('express')
const router = express.Router()

router.get('/', messageController.message_list)
router.delete('/:id', messageController.message_remove)

module.exports = router;
