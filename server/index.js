#!/usr/bin/env node

const express = require("express");
const app = express();
const cors = require("cors");
const port = 8000
const path = require('path')
const http = require('http')

app.use(express.json());
app.use('/', express.static(path.join(__dirname, 'static')))
app.use(express.urlencoded({extended: true}))

const indexRouter = require('./routes/index');
const vehiclesRouter = require('./routes/vehicles');
const tagsRouter = require('./routes/tags');

app.use('/api/', indexRouter);
app.use('/api/vehicles', vehiclesRouter);
app.use('/api/tags', tagsRouter);

app.use(cors());

app.listen(port, () => console.log("The node.js app is listening on port 80."));

