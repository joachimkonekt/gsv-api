'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Consumptions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      vehicle_id: {
        type: Sequelize.UUID,
        references: {model: 'vehicles', key: 'id'}
      },
      fuel_cons_day: {
        type: Sequelize.STRING
      },
      usage_hours_day: {
        type: Sequelize.STRING
      },
      co2_day_kg: {
        type: Sequelize.STRING
      },
      date: {
        type: Sequelize.TIME
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Consumptions');
  }
};
