'use strict';
module.exports = (sequelize, DataTypes) => {
    var Cumulative_idle_hours = sequelize.define('Cumulative_idle_hours', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        vehicle_id: DataTypes.UUID,
        count: DataTypes.REAL,
        date: DataTypes.TIME
    }, {});
    Cumulative_idle_hours.associate = function(models) {
        // associations can be defined here
    };
    return Cumulative_idle_hours;
};
