'use strict';
module.exports = (sequelize, DataTypes) => {
  var Fuel_remaining = sequelize.define('Fuel_remaining', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true
    },
    vehicle_id: DataTypes.UUID,
    percent: DataTypes.REAL,
    date: DataTypes.TIME
  }, {});
  Fuel_remaining.associate = function(models) {
    // associations can be defined here
  };
  return Fuel_remaining;
};
