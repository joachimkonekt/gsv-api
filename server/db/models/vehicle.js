'use strict';
module.exports = (sequelize, DataTypes) => {
    let Vehicle = sequelize.define('Vehicle', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true
        },
        gsv_id: DataTypes.STRING,
        trackunit_id: DataTypes.STRING,
        oem: DataTypes.STRING,
        model: DataTypes.STRING,
        serial_number: DataTypes.STRING
    }, {});
    Vehicle.associate = function(models) {
        Vehicle.hasMany(models.Consumption, {
            foreignKey: "vehicle_id",
            // as: "Consumptions"
        })
    };
    return Vehicle;
};
