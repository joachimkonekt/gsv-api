'use strict';
module.exports = (sequelize, DataTypes) => {
  var Cumulative_operating_hours = sequelize.define('Cumulative_operating_hours', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true
    },
    vehicle_id: DataTypes.UUID,
    count: DataTypes.REAL,
    date: DataTypes.TIME
  }, {});
  Cumulative_operating_hours.associate = function(models) {
    // associations can be defined here
  };
  return Cumulative_operating_hours;
};
