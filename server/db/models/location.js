'use strict';
module.exports = (sequelize, DataTypes) => {
  var Location = sequelize.define('Location', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    coordinates: DataTypes.STRING,
    vehicle_id: DataTypes.UUID,
    time: DataTypes.TIME
  }, {});
  Location.associate = function(models) {
    // associations can be defined here
  };
  return Location;
};
