'use strict';
module.exports = (sequelize, DataTypes) => {
  var Consumption = sequelize.define('Consumption', {
    id:{
        type: DataTypes.UUID,
        primaryKey: true
    },
    vehicle_id: DataTypes.UUID,
    fuel_cons_day: DataTypes.STRING,
    usage_hours_day: DataTypes.STRING,
    co2_day_kg: DataTypes.STRING,
    date: DataTypes.TIME
  }, {});
  Consumption.associate = function(models) {
    Consumption.belongsTo(models.Vehicle, {
      foreignKey: "vehicle_id",
      // as: "vehicleConsumption"
    })
  };
  return Consumption;
};
