'use strict';
module.exports = (sequelize, DataTypes) => {
  var Fuel_usage = sequelize.define('Fuel_usage', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    vehicle_id: DataTypes.UUID,
    units: DataTypes.STRING,
    consumed: DataTypes.INTEGER
  }, {});
  Fuel_usage.associate = function(models) {
    // associations can be defined here
  };
  return Fuel_usage;
};
