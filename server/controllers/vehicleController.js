const uuidv4 = require('uuid/v4');
const vehicleModel = require('../db/models').Vehicle;
const Consumption = require('../db/models').Consumption;

exports.vehicle_list = (req, res) => {
    return vehicleModel.findAndCountAll({
        include: {
            model: Consumption,
            as: 'Consumption'
        }
        // include: [{ model: Consumption }]
    })
        .then(
            data => {
                res.send(data);
            }
        )
        .catch(error => res.status(400).send(error));
}

exports.vehicle_detail = (req, res) => {
    vehicleModel.findOne({
        where: { id: req.params.id },
        include: {
            model: Consumption,
            // as: 'Consumptions'
        }
    }).then(vehicle => {
        if (!vehicle) {
            return res.sendStatus(404);
        }
        res.send(vehicle)
    }).catch(err => console.log(err.message));
}

exports.vehicle_create_mass = (req, res) => {
    console.log('>>>')
}

exports.vehicle_create_post = (req, res) => {
    const {gsv_id, trackunit_id, oem, model, serial_number} = req.body

    return vehicleModel.create({
        id: uuidv4(),
        gsv_id,
        trackunit_id,
        oem,
        model,
        serial_number
    })
        .then(vehicleModel => res.status(201).send(vehicleModel))
        .catch(error => res.status(400).send(error));
}

exports.vehicle_update_put = (req, res) => {

    vehicleModel.update(req.body, {
        where: {id: req.params.id}
    }).then(num => {
        if (num == 1) {
            res.send({
                message: "Vehicle was updated successfully."
            });
        } else {
            res.send({
                message: `Cannot update Vehicle with id=${id}. Maybe Vehicle was not found or req.body is empty!`
            });
        }
    })

}

exports.vehicle_delete_post = (req, res) => {
    vehicleModel.destroy({
        where: {id: req.params.id}
    })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Vehicle was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Vehicle with id=${id}. Maybe Vehicle was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Vehicle with id=" + id
      });
    });


}

exports.vehicle_add_consumption = (req, res) => {
    const {fuel_cons_day, usage_hours_day, co2_day_kg, date} = req.body

    return Consumption.create({
        id: uuidv4(),
        vehicle_id: req.params.id,
        fuel_cons_day,
        usage_hours_day,
        co2_day_kg,
        date
    })
        .then(Consumption => res.status(201).send(Consumption))
        .catch(error => res.status(400).send(error));
}

exports.vehicle_assign_tag_post = (req, res) => {
    //TODO implement the method
}

exports.vehicle_detach_tag_delete = (req, res) => {
    //TODO implement the method
}

exports.vehicle_get_tags = (req, res) => {
    //TODO implement the method
}

exports.vehicle_location_report = (req, res) => {
    //TODO implement the method
}

exports.vehicle_utilization_report = (req, res) => {
    //TODO implement the method
}

exports.vehicle_location = (req, res) => {
    //TODO implement the method
}

exports.vehicle_utilization = (req, res) => {
    //TODO implement the method
}
